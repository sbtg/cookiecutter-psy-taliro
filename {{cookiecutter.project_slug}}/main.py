from __future__ import annotations

from collections.abc import Sequence
from typing import Any

import numpy as np
from numpy.typing import NDArray
from staliro.models import blackbox
from staliro.options import Options
from staliro.staliro import staliro
{%- if cookiecutter.backend == "TLTk" %}
from staliro.specifications import TLTK
{%- elif cookiecutter.backend == "RTAMT Dense" %}
from staliro.specifications import RTAMTDense
{%- elif cookiecutter.backend == "RTAMT Discrete" %}
from staliro.specifications import RTAMTDiscrete
{% endif %}
from staliro.optimizers import Behavior
{%- if cookiecutter.optimizer == "Uniform random" %}
from staliro.optimizers import UniformRandom
{%- elif cookiecutter.optimizer == "Simulated annealing" %}
from staliro.optimizers import DualAnnealing
{% endif %}
from staliro.core.model import ModelData, ModelResult, Failure

StaticT = Sequence[float]
TimesT = NDArray[np.float64]
SignalsT = NDArray[np.float64]

@blackbox()
def model(static: StaticT, times: TimesT, signals: SignalsT) -> ModelResult[T, U]:
    # TODO: Implement model
    # A model is parameterized by the types StateT and ExtraT which represent the type of the state data returned by
    # the model and the type of any user-defined extra data
    # The model must return an instance of ModelData or Failure with the state data and optional extra data

    raise NotImplementedError()


if __name__ == "__main__":
    options = Options()  # TODO: Define options

    formula = ""
    predicates = {}
    {%- if cookiecutter.backend == "TLTk" %}
    specification = TLTK(formula, predicates)
    {%- elif cookiecutter.backend == "RTAMT Dense" %}
    specification = RTAMTDense(formula, predicates)
    {%- elif cookiecutter.backend == "RTAMT Discrete" %}
    specification = RTAMTDiscrete(formula, predicates)
    {% endif %}
    {% if cookiecutter.optimizer_behavior == "Falsification" %}
    behavior = Behavior.FALSIFICATION
    {%- elif cookiecutter.optimizer_behavior == "Minimization" %}
    behavior = Behavior.MINIMIZATION
    {%- endif %}
    {%- if cookiecutter.optimizer == "Uniform random" %}
    optimizer = UniformRandom(parallelization=None, behavior=behavior)
    {%- elif cookiecutter.optimizer == "Simulated annealing" %}
    optimizer = DualAnnealing(behavior)
    {% endif %}

    result = staliro(model, specification, optimizer, options)
    # TODO: process results
