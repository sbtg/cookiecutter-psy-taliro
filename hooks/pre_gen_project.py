import sys

backend_name = "{{ cookiecutter.backend }}"

if __name__ == "__main__":
    if backend_name == "TLTk" and not sys.platform.startswith("linux"):
        raise RuntimeError("TLTk is only supported on Linux platforms")
